#!/bin/bash
set -e
# This script builds the actual image.
# Works on chroot.
# Depends on qemu-arm-static.
IMAGE=$1

if [ "$IMAGE" = "" ]; then
  echo "Error: You need to specify an image."
  exit 1
fi

# First, let's expand the image so we have space:
if ! [ -n "$(find "$IMAGE" -prune -size +1600M)" ]; then
  dd if=/dev/zero bs=1M count=1600 >> $IMAGE
  losetup -f -P $IMAGE
  LOOP_DEV=/dev/$(lsblk | grep loop | grep -v "-" | cut -d' ' -f1 | tail -1)
  losetup -c $LOOP_DEV
  e2fsck -yf "${LOOP_DEV}p2"
  echo ", +" | sfdisk -N 2 $LOOP_DEV
  resize2fs "${LOOP_DEV}p2"
  losetup -d $LOOP_DEV
fi

# Mount the image
if ! mount | grep rospi-mnt > /dev/null; then
  echo "Mounting FSes..."
  mkdir -p rospi-mnt

  losetup -f -P $IMAGE
  LOOP_DEV=/dev/$(lsblk | grep loop | grep -v "-" | cut -d' ' -f1 | tail -1)

  mount "${LOOP_DEV}p2" rospi-mnt

  # Mount needed sysfs directories
  mount --bind /dev rospi-mnt/dev/
  mount --bind /dev/pts rospi-mnt/dev/pts
  mount --bind /sys rospi-mnt/sys/
  mount --bind /proc rospi-mnt/proc/
  mount --bind /run rospi-mnt/run/
  mount --make-rslave rospi-mnt/dev
  mount --make-rslave rospi-mnt/dev/pts
  mount --make-rslave rospi-mnt/sys
  mount --make-rslave rospi-mnt/proc
  mount --make-rslave rospi-mnt/run
fi

# Copy QEMU (for amd64 -> arm64)
echo "Copying QEMU..."
cp /usr/bin/qemu-arm-static  rospi-mnt/bin

# Copy network configs
echo "Copying configs and scripts..."
mkdir -p rospi-mnt/run/systemd/resolve
# TODO: As of testing DNS resolving isn't working (might be a local issue?)
# cp /etc/resolv.conf rospi-mnt/run/systemd/resolve/stub-resolv.conf
cp -a install-ros.sh rospi-mnt/root/install-ros.sh
cp -a install-mavsdk.sh rospi-mnt/root/install-mavsdk.sh
cp -a install-de.sh rospi-mnt/root/install-de.sh

# # Run the install
chroot rospi-mnt /root/install-ros.sh
chroot rospi-mnt /root/install-mavsdk.sh

# If we used the etc hosts hack, unhack it
HOSTS_HACK=0
if grep "HACKY HACK" rospi-mnt/etc/hosts; then
  HOSTS_HACK=1
  sed -i '9,$ d' rospi-mnt/etc/hosts
  echo "Removing /etc/hosts hack..."
fi

# Copy lite image
NEW_IMAGE=pipx4-$(date -I)-lite.img
cp $IMAGE $NEW_IMAGE

echo "Compressing image ${NEW_IMAGE}..."
xz -T4 $NEW_IMAGE

# --- FULL/DESKTOP INSTALL --- #
# Expand image further
LOOP_DEV=/dev/$(lsblk | grep loop | grep -v "-" | cut -d' ' -f1 | tail -1)

if ! [ -n "$(find "$IMAGE" -prune -size +2600M)" ]; then
  dd if=/dev/zero bs=1M count=1000 >> $IMAGE
  losetup -c $LOOP_DEV
  e2fsck -yf "${LOOP_DEV}p2"
  echo ", +" | sfdisk -N 2 $LOOP_DEV
  resize2fs "${LOOP_DEV}p2"
fi

# Re-apply hack
if [ $HOSTS_HACK -eq 1 ]; then
  ./etc-hosts.sh
fi

# Install desktop
chroot rospi-mnt /root/install-de.sh

# Clean up
# If we used the etc hosts hack, unhack it
HOSTS_HACK=0
if grep "HACKY HACK" rospi-mnt/etc/hosts; then
  HOSTS_HACK=1
  sed -i '9,$ d' rospi-mnt/etc/hosts
  echo "Removing /etc/hosts hack..."
fi

echo "Unmounting ${LOOP_DEV}..."
umount -R rospi-mnt
losetup -d $LOOP_DEV

# Compress the image
NEW_IMAGE=pipx4-$(date -I)-full.img
cp $IMAGE $NEW_IMAGE

echo "Compressing image ${NEW_IMAGE}..."
xz -T4 $NEW_IMAGE
