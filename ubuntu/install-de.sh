#!/bin/bash
# Credit: https://github.com/TerraGitHuB/gnomeforpi

echo "Installing GNOME..."

echo "Updating Repositories"
sleep 2
apt-get update
apt-get upgrade -y

sudo apt install -y gnome-session gnome-shell nautilus gedit gnome-terminal seahorse gnome-tweaks

# echo "Disabling dhcpcd to be able to connect to wifi via gnome network manager"
# sleep 2
# systemctl disable dhcpcd && /etc/init.d/dhcpcd stop
#
# echo "Correcting Audio for GNOME"
# sleep 2
# sudo sed -i 's/load-module module-udev-detect/load-module module-udev-detect tsched=0/g' /etc/pulse/default.pa
#
# #echo "Disable Animations"
# #sleep 2
# #sudo gsettings set org.gnome.desktop.interface enable-animations false
# #wait
#
# echo "change permission settings for GNOME"
# sleep 2
# sudo touch /etc/polkit-1/localauthority.conf.d/51-admin.conf
# sudo sh -c 'echo "[Configuration]" >> /etc/polkit-1/localauthority.conf.d/51-admin.conf'
# sudo sh -c 'echo "AdminIdentities=unix-group:sudo;unix-group:admin" >> /etc/polkit-1/localauthority.conf.d/51-admin.conf'
