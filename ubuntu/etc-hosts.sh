#!/bin/bash
# Write /etc/hosts for needed domains
# TODO: This really shouldn't be necessary, but for some reason
# (on my system at least) DNS insists on not working, so here's a solution
echo "# HACKY HACK BAD" >> rospi-mnt/etc/hosts
echo "91.189.88.142 ports.ubuntu.com" >> rospi-mnt/etc/hosts
echo "192.30.255.113 github.com" >> rospi-mnt/etc/hosts
echo "151.101.40.133 raw.githubusercontent.com" >> rospi-mnt/etc/hosts
echo "64.50.236.52 packages.ros.org" >> rospi-mnt/etc/hosts
echo "151.101.40.223 pypi.python.org" >> rospi-mnt/etc/hosts
echo "151.101.192.223 pypi.org" >> rospi-mnt/etc/hosts
echo "151.101.41.63 files.pythonhosted.org" >> rospi-mnt/etc/hosts
