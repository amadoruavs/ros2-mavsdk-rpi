#!/bin/bash -e
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list
echo "$(echo -n 'deb http://mirrors.ocf.berkeley.edu/raspbian/raspbian buster main contrib non-free rpi'; cat /etc/apt/sources.list)" > /etc/apt/sources.list
apt update
