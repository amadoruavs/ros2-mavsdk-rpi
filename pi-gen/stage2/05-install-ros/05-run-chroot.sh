#!/bin/bash -e

cd /root
mkdir -p /opt/ros/foxy

wget https://github.com/ros2/ros2/releases/download/release-foxy-20201211/ros2-foxy-20201211-linux-focal-arm64.tar.bz2
tar xf ros2-foxy-20201211-linux-focal-arm64.tar.bz2

apt update
apt install -y python3-rosdep
rosdep init
rosdep update

rosdep install --from-paths ros2-linux/share --ignore-src --rosdistro foxy -y --skip-keys "console_bridge fastcdr fastrtps osrf_testing_tools_cpp poco_vendor rmw_connext_cpp rosidl_typesupport_connext_c rosidl_typesupport_connext_cpp rti-connext-dds-5.3.1 tinyxml_vendor tinyxml2_vendor urdfdom urdfdom_headers cyclonedds"

apt install -y libpython3-dev python3-pip
pip3 install -U argcomplete

rm -rf ros2-foxy-20201211-linux-focal-arm64.tar.bz2 ros2-linux
