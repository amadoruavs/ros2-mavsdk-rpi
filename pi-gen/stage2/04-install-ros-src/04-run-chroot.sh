#!/bin/bash -e
mkdir -p /root/ros2_foxy/src
cd /root/ros2_foxy

wget https://raw.githubusercontent.com/ros2/ros2/foxy/ros2.repos
vcs import src < ros2.repos

rosdep init
rosdep update
rosdep install --from-paths src --ignore-src --rosdistro foxy -y --skip-keys "console_bridge fastcdr fastrtps rti-connext-dds-5.3.1 urdfdom_headers"

cd /root/ros2_foxy/
colcon build --symlink-install
colcon build --symlink-install --merge-install

mkdir -p /opt/ros/foxy
mv /root/ros2_foxy/install /opt/ros/foxy
cd /root
rm -rf /root/ros2_foxy
